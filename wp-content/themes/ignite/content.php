<?php

$date_display = get_theme_mod( 'ct_ignite_post_meta_date_settings' );
$author_display = get_theme_mod( 'ct_ignite_post_meta_author_settings' );
global $post;
$previous_post = get_adjacent_post(false,'',true);
$previous_link = get_permalink( $previous_post );
		    
if( is_single() ) { ?>
    <div <?php post_class(); ?>>
        <?php
        if( $date_display != 'hide' || $author_display != 'hide' ) : ?>
            <div class="entry-meta-top">
                <?php
                // Don't display if hidden by Post Meta section
                if( $date_display != 'hide' ) {
                    echo __( 'Published', 'ignite' ) . " " . date_i18n( get_option( 'date_format' ), strtotime( get_the_date('r') ) );
                }
                // output author name/link if not set to "Hide" in Post Meta section
                if( $author_display != 'hide' ) {

                    // if the date is hidden, capitalize "By"
                    if( $date_display == 'hide' ) {
                        echo " " . ucfirst( _x( 'by', 'Published by whom?', 'ignite' ) ) . " ";
                        the_author_posts_link();
                    } else {
                        echo " " . _x( 'by', 'Published by whom?', 'ignite' ) . " ";
                        the_author_posts_link();
                    }
                }
                ?>
            </div>
        <?php endif; ?>
        <div class='entry-header'>
            <h1 class='entry-title'><?php the_title(); ?></h1>
        </div>
        <div class="entry-content">
            <article>
                <div class="mainContentAdsense"><i>Anzeige</i>


<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- baufun_topcontent -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-3239792510565110"
     data-ad-slot="9187226171"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>




                </div>

                <?php the_content(); ?>
          <span class="bfmash">
                    <?php echo do_shortcode('[mashshare]'); ?>

                </span>
		<span class="bfnext">
		    <a href="<?php echo $previous_link ?>">Weiter</a>
		</span>
                <?php wp_link_pages(array('before' => '<div class="moreofthis">Auf der nächsten Seite geht es weiter…</div><span class="singular-pagination bfcenter">' . __(''), 'next_or_number'=>'next', 'previouspagelink' => ' <i>&lt;</i> &nbsp;&nbsp;' , 'nextpagelink'=>'&nbsp;&nbsp; <i>&gt;</i>', 'after' => '</span>')); ?>

                <div class="picAdsense" style="clear:both;">      
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- baufun_content_bottom -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3239792510565110"
     data-ad-slot="9419668571"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
                </div>

      
                <span>
                    <?php if (class_exists('plista')) { echo plista::plista_integration ($content); } ?>
		    <?php //echo do_shortcode( '[plista widgetname="plista_widget_standard_1"]' );?>
		</span>

            </article>
        </div>
        <div class='entry-meta-bottom'>
            <?php
            if( get_theme_mod('ct_ignite_post_meta_further_reading_settings') != 'hide' ) {
                get_template_part('content/further-reading');
            }
            ?>
            <?php
            if(get_theme_mod('ct_ignite_author_meta_settings') != 'hide'){ ?>
                <div class="author-meta">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 72, '', get_the_author() ); ?>
                    <div class="name-container">
                        <h4>
                            <?php
                            if(get_the_author_meta('user_url')){
                                echo "<a href='" . get_the_author_meta('user_url') . "'>" . get_the_author() . "</a>";
                            } else {
                                the_author();
                            }
                            ?>
                        </h4>
                    </div>
                    <p><?php echo get_the_author_meta('description'); ?></p>
                </div>
            <?php } ?>
            <?php
            if(get_theme_mod('ct_ignite_post_meta_categories_settings') != 'hide'){ ?>
                <div class="entry-categories"><?php get_template_part('content/category-links'); ?></div><?php
            }
            if(get_theme_mod('ct_ignite_post_meta_tags_settings') != 'hide'){ ?>
                <div class="entry-tags"><?php get_template_part('content/tag-links'); ?></div><?php
            }
            ?>
        </div>
    </div>
<?php

} else { ?>

    <div <?php post_class(); ?>>
        <?php ct_ignite_featured_image(); ?>
        <?php
        // as long as one isn't equal to 'hide', display entry-meta-top
        if( $date_display != 'hide' || $author_display != 'hide' ) : ?>
            <div class="entry-meta-top">
                <?php
                // Don't display if hidden by Post Meta section
                if( $date_display != 'hide' ) {
                    echo __( 'Published', 'ignite' ) . " " . date_i18n( get_option( 'date_format' ), strtotime( get_the_date('r') ) );
                }
                // output author name/link if not set to "Hide" in Post Meta section
                if( $author_display != 'hide' ) {

                    // if the date is hidden, capitalize "By"
                    if( $date_display == 'hide' ) {
                        echo " " . ucfirst( _x( 'by', 'Published by whom?', 'ignite' ) ) . " ";
                        the_author_posts_link();
                    } else {
                        echo " " . _x( 'by', 'Published by whom?', 'ignite' ) . " ";
                        the_author_posts_link();
                    }
                }
                ?>
            </div>
        <?php endif; ?>
        <div class='excerpt-header'>
            <h1 class='excerpt-title'>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h1>
        </div>
        <div class='excerpt-content'>
            <article>
                <?php ct_ignite_excerpt(); ?>
            </article>
        </div>

        <?php
        if(get_theme_mod('ct_ignite_post_meta_tags_settings') != 'hide'){ ?>
            <div class="entry-tags"><?php get_template_part('content/tag-links'); ?></div><?php
        }
        ?>
    </div>
<?php
}