</div> <!-- .main -->

<?php if( ! is_page_template('full-width.php') ) : ?>
	<div id="sidebar-primary-container" class="sidebar-primary-container">
	    <?php get_sidebar( 'primary' ); ?>
	</div>
<?php endif; ?>

</div> <!-- .overflow-container -->

<footer class="site-footer" role="contentinfo">
    <h3><a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('title'); ?></a></h3>
    <span><?php bloginfo('description'); ?></span>
    <div class="design-credit">
        <span>
            <?php
            /* Get the user's footer text input */
            $user_footer_text = get_theme_mod('ct_ignite_footer_text_setting');

            /* If it's not empty, output their text */
            if( ! empty($user_footer_text) ) {
                echo wp_kses_post( $user_footer_text );
            }
            /* Otherwise, output the default text */
            else {

            }
            ?>
        </span>
    </div>
</footer>

<?php wp_footer(); ?> 
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-47558501-2', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>