<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <?php wp_head(); ?>

    <script type="text/javascript" src="/wp-content/themes/ignite/js/bf.js"></script>

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-3239792510565110",
            enable_page_level_ads: true
        });
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-47558501-2', 'auto');
      ga('send', 'pageview');

    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5612c2fa40fa9c00" async="async"></script>


</head>

<body id="<?php print get_stylesheet(); ?>" <?php body_class(); ?>>

<!--skip to content link-->
<a class="skip-content" href="#main"><?php _e('Skip to content', 'ignite'); ?></a>

<header class="site-header" id="site-header" role="banner">

	<div id="title-info" class="title-info">
		<?php get_template_part('logo')  ?>
	</div>
	
	<?php get_template_part( 'menu', 'primary' ); ?>

</header>
<div id="overflow-container" class="overflow-container">

    <?php

    // if not 'no' so it falls back to usage of on default/unset
    if(get_theme_mod('ct_ignite_show_breadcrumbs_setting') != 'no') {
        ct_ignite_breadcrumbs();
    }
    ?>
    <div id="main" class="main" role="main">