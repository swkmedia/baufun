</div> <!-- .main -->

<?php if( ! is_page_template('full-width.php') ) : ?>
	<div id="sidebar-primary-container" class="sidebar-primary-container">
	    <?php get_sidebar( 'primary' ); ?>
	</div>
<?php endif; ?>

</div> <!-- .overflow-container -->

<footer class="site-footer" role="contentinfo">
    <h3><a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('title'); ?></a></h3>
    <span><?php bloginfo('description'); ?></span>
    <div class="design-credit">
        <span>
            <?php
            /* Get the user's footer text input */
            $user_footer_text = get_theme_mod('ct_ignite_footer_text_setting');

            /* If it's not empty, output their text */
            if( ! empty($user_footer_text) ) {
                echo wp_kses_post( $user_footer_text );
            }
            /* Otherwise, output the default text */
            else {

                echo '<div class="footerMenu">
                            <ul>
                                <li><a href="http://www.baufun.de/kontakt">Kontakt</a></li>
                                <li><a href="http://www.baufun.de/datenschutz">Datenschutz</a></li>
                                <li><a href="http://www.baufun.de/impressum">Impressum</a></li>
                            </ul>
                      </div>';
            }
            ?>
        </span>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>