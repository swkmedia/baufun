<?php
/**
 * CRON functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

if ( ! wp_next_scheduled( 'bimber_update_hot_posts' ) ) {
	wp_schedule_event( time(), 'daily', 'bimber_update_hot_posts' );
}

if ( ! wp_next_scheduled( 'bimber_update_popular_posts' ) ) {
	wp_schedule_event( time(), 'daily', 'bimber_update_popular_posts' );
}

if ( ! wp_next_scheduled( 'bimber_update_trending_posts' ) ) {
	wp_schedule_event( time(), 'daily', 'bimber_update_trending_posts' );
}