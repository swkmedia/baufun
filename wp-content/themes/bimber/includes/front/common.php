<?php
/**
 * Front common functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Get logo image.
 *
 * @return array
 */
function bimber_get_logo() {
	$result = array();

	$logo = bimber_get_theme_option( 'branding', 'logo' );

	if ( empty( $logo ) ) {
		return array();
	}

	$result['src'] = $logo;

	$logo_2x = bimber_get_theme_option( 'branding', 'logo_hdpi' );

	if ( ! empty( $logo_2x ) ) {
		$result['srcset'] = $logo_2x . ' 2x,' . $logo . ' 1x';
	}

	$result['width']  = bimber_get_theme_option( 'branding', 'logo_width' );
	$result['height'] = bimber_get_theme_option( 'branding', 'logo_height' );

	return $result;
}

/**
 * Get small logo image.
 *
 * @return array
 */
function bimber_get_small_logo() {
	$result = array();

	$logo = bimber_get_theme_option( 'branding', 'logo_small' );

	if ( empty( $logo ) ) {
		return array();
	}

	$result['src'] = $logo;

	$logo_2x = bimber_get_theme_option( 'branding', 'logo_small_hdpi' );

	if ( ! empty( $logo_2x ) ) {
		$result['srcset'] = $logo_2x . ' 2x,' . $logo . ' 1x';
	}

	$result['width']  = bimber_get_theme_option( 'branding', 'logo_small_width' );
	$result['height'] = bimber_get_theme_option( 'branding', 'logo_small_height' );

	return $result;
}

/**
 * Get microdata organization logo URL.
 */
function bimber_get_microdata_organization_logo_url() {
	$url = bimber_get_theme_option( 'branding', 'logo_hdpi' );

	return $url;
}


/**
 * Get footer stamp image.
 *
 * @return array
 */
function bimber_get_footer_stamp() {
	$result = array();

	$stamp = bimber_get_theme_option( 'footer', 'stamp' );

	if ( empty( $stamp ) ) {
		return array();
	}

	$result['src'] = $stamp;

	$stamp_2x = bimber_get_theme_option( 'footer', 'stamp_hdpi' );

	if ( ! empty( $stamp_2x ) ) {
		$result['srcset'] = $stamp_2x;
	}

	$result['width']  = bimber_get_theme_option( 'footer', 'stamp_width' );
	$result['height'] = bimber_get_theme_option( 'footer', 'stamp_height' );

	return $result;
}


/**
 * Set template part data.
 *
 * @param mixed|void $data Data.
 */
function bimber_set_template_part_data( $data ) {
	global $bimber_template_data;
	global $bimber_template_data_last;

	$bimber_template_data_last = $bimber_template_data;
	$bimber_template_data      = $data;
}

/**
 * Get template part data.
 *
 * @return mixed
 */
function bimber_get_template_part_data() {
	global $bimber_template_data;

	return $bimber_template_data;
}

/**
 * Reset template part data
 */
function bimber_reset_template_part_data() {
	global $bimber_template_data;
	global $bimber_template_data_last;

	$bimber_template_data = $bimber_template_data_last;
}

/**
 * Enqueue stylesheets in the head
 */
function bimber_enqueue_head_styles() {
	// Prevent CSS|JS caching during updates.
	$version = bimber_get_theme_version();
	$uri     = trailingslashit( get_template_directory_uri() );

	$variant         = 'main';
	$variant_css_id  = 'g1-' . $variant;
	$variant_css_uri = $uri . 'css/' . $variant . '.css';
	wp_enqueue_style( $variant_css_id, $variant_css_uri, array(), $version );
	wp_style_add_data( $variant_css_id, 'rtl', 'replace' );

	// @todo - Move it to the appropriate place
	if ( bimber_can_use_plugin( 'snax/snax.php' ) ) {
		wp_enqueue_style( 'bimber-snax-extra', $uri . 'css/snax-extra.css', array(), $version );
		wp_style_add_data( 'bimber-snax-extra', 'rtl', 'replace' );
	}

	wp_enqueue_style( 'bimber-google-fonts', bimber_google_fonts_url(), array(), $version );

	wp_enqueue_style( 'bimber-print', $uri . 'css/print.css', array(), $version, 'print' );

	if ( bimber_use_external_dynamic_style() ) {
		wp_enqueue_style( 'bimber-dynamic-style', bimber_dynamic_style_get_file_url() . '?respondjs=no', array(), $version );
	}

	// Load the stylesheet from the child theme.
	if ( get_template_directory() !== get_stylesheet_directory() ) {
		wp_register_style( 'bimber-style', get_stylesheet_uri(), array(), false, 'screen' );
		wp_style_add_data( 'bimber-style', 'rtl', trailingslashit( get_stylesheet_directory_uri() ) . 'rtl.css' );
		wp_enqueue_style( 'bimber-style' );
	}
}

/**
 * Google fonts
 */
function bimber_google_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Roboto, translate this to 'off'. Do not translate into your own language.
	 */
	$roboto = _x( 'on', 'Roboto font: on or off', 'bimber' );

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Poppins, translate this to 'off'. Do not translate into your own language.
	 */
	$poppins = _x( 'on', 'Poppins font: on or off', 'bimber' );

	if ( 'off' !== $roboto || 'off' !== $poppins ) {
		$font_families = array();

		if ( 'off' !== $roboto ) {
			$font_families[] = 'Roboto:400,300,700';
		}

		if ( 'off' !== $poppins ) {
			$font_families[] = 'Poppins:400,300,500,600,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( bimber_get_google_font_subset() ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Get Google Font font subset
 *
 * @return string
 */
function bimber_get_google_font_subset() {
	$subset = bimber_get_theme_option( 'global', 'google_font_subset' );

	return apply_filters( 'bimber_google_font_subset', $subset );
}

/**
 * Render scripts in the HTML head
 */
function bimber_render_head_scripts() {
}

/**
 * Enqueue scripts used only on the frontend
 */
function bimber_enqueue_front_scripts() {
	// Prevent CSS|JS caching during updates.
	$version = bimber_get_theme_version();

	$parent_uri = trailingslashit( get_template_directory_uri() );
	$child_uri  = trailingslashit( get_stylesheet_directory_uri() );

	wp_enqueue_script( 'jquery' );

	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * Head scripts.
	 */

	wp_enqueue_script( 'modernizr', $parent_uri . 'js/modernizr/modernizr-custom.min.js', array(), '3.3.0', false );

	/**
	 * Footer scripts.
	 */

	// Enqueue input::placeholder polyfill for IE9.
	wp_enqueue_script( 'jquery-placeholder', $parent_uri . 'js/jquery.placeholder/placeholders.jquery.min.js', array( 'jquery' ), '4.0.1', true );

	// Conver dates into fuzzy timestaps.
	wp_enqueue_script( 'jquery-timeago', $parent_uri . 'js/jquery.timeago/jquery.timeago.js', array( 'jquery' ), '1.5.2', true );
	bimber_enqueue_timeago_i10n_script( $parent_uri );

	// Enqueue matchmedia polyfill.
	wp_enqueue_script( 'match-media', $parent_uri . 'js/matchMedia/matchMedia.js', array(), null, true );

	// Enqueue matchmedia addListener polyfill (media query events on window resize) for IE9.
	wp_enqueue_script( 'match-media-add-listener', $parent_uri . 'js/matchMedia/matchMedia.addListener.js', array( 'match-media' ), null, true );

	// Enqueue <picture> polyfill, <img srcset="" /> polyfill for Safari 7.0-, FF 37-, etc.
	wp_enqueue_script( 'picturefill', $parent_uri . 'js/picturefill/picturefill.min.js', array( 'match-media' ), '2.3.1', true );

	// Scroll events.
	wp_enqueue_script( 'jquery-waypoints', $parent_uri . 'js/jquery.waypoints/jquery.waypoints.min.js', array( 'jquery' ), '4.0.0', true );

	// GifPlayer.
	wp_enqueue_script( 'libgif', $parent_uri . 'js/libgif/libgif.js', array(), null, true );

	// Media queries in javascript.
	wp_enqueue_script( 'enquire', $parent_uri . 'js/enquire/enquire.min.js', array( 'match-media', 'match-media-add-listener' ), '2.1.2', true );

	wp_enqueue_script( 'bimber-front', $parent_uri . 'js/front.js', array( 'jquery', 'enquire' ), $version, true );
	wp_enqueue_script( 'bimber-menu', $parent_uri . 'js/menu.js', array( 'bimber-front' ), $version, true );

	// If child theme is activated, we can use this script to override theme js code.
	if ( $parent_uri !== $child_uri ) {
		wp_enqueue_script( 'bimber-child', $child_uri . 'modifications.js', array( 'bimber-front' ), null, true );
	}

	// Prepare js config.
	$config = array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'timeago'  => bimber_get_theme_option( 'posts', 'timeago', 'standard' ) === 'standard' ? 'on' : 'off',
		'sharebar' => bimber_get_theme_option( 'post', 'sharebar', 'standard' ) === 'standard' ? 'on' : 'off',
		'i10n'     => array(
			'subscribe_mail_subject_tpl' => esc_html__( 'Check out this great article: %subject%', 'bimber' ),
		),
	);

	wp_localize_script( 'bimber-front', 'bimber_front_config', wp_json_encode( $config ) );
}

/**
 * Enqueue translation file for the timeago script
 *
 * @param string $parent_uri Parent Theme URI.
 */
function bimber_enqueue_timeago_i10n_script( $parent_uri ) {
	$locale       = get_locale();
	$locale_parts = explode( '_', $locale );
	$lang_code    = $locale_parts[0];

	$exceptions_map = array(
		'pt_BR' => 'pt-br',
		'zh_CN' => 'zh-CN',
		'zh_TW' => 'zh-TW',
	);

	$script_i10n_ext = $lang_code;

	if ( isset( $exceptions_map[ $locale ] ) ) {
		$script_i10n_ext = $exceptions_map[ $locale ];
	}

	// Check if translation file exists in "locales" dir.
	if ( ! file_exists( BIMBER_THEME_DIR . 'js/jquery.timeago/locales/jquery.timeago.' . $script_i10n_ext . '.js' ) ) {
		return;
	}

	wp_enqueue_script( 'jquery-timeago-' . $script_i10n_ext, $parent_uri . 'js/jquery.timeago/locales/jquery.timeago.' . $script_i10n_ext . '.js', array( 'jquery-timeago' ), null, true );
}

/**
 * Load front scripts conditionally
 *
 * @param string $tag Script tag.
 * @param string $handle Script handle.
 *
 * @return string
 */
function bimber_load_front_scripts_conditionally( $tag, $handle ) {
	if ( in_array( $handle, array( 'placeholder' ), true ) ) {
		$tag = "\n<!--[if IE 9]>\n$tag<![endif]-->\n";
	}

	return $tag;
}

/**
 * Render meta tag with proper viewport.
 */
function bimber_add_responsive_design_meta_tag() {
	echo "\n" . '<meta name="viewport" content="initial-scale=1.0, width=device-width" />' . "\n";
}

/**
 * Alter the HTML markup of the calendar widget.
 *
 * @param string $out Markup.
 *
 * @return string
 */
function bimber_alter_calendar_output( $out ) {
	$out = str_replace(
		array(
			'<td class="pad" colspan="1">&nbsp;</td>',
			'<td class="pad" colspan="2">&nbsp;</td>',
			'<td class="pad" colspan="3">&nbsp;</td>',
			'<td class="pad" colspan="4">&nbsp;</td>',
			'<td class="pad" colspan="5">&nbsp;</td>',
			'<td class="pad" colspan="6">&nbsp;</td>',
			'<td colspan="1" class="pad">&nbsp;</td>',
			'<td colspan="2" class="pad">&nbsp;</td>',
			'<td colspan="3" class="pad">&nbsp;</td>',
			'<td colspan="4" class="pad">&nbsp;</td>',
			'<td colspan="5" class="pad">&nbsp;</td>',
			'<td colspan="6" class="pad">&nbsp;</td>',
			'<td colspan="3" id="prev" class="pad">&nbsp;</td>',
			'<td colspan="3" id="next" class="pad">&nbsp;</td>',
		),
		array(
			str_repeat( '<td class="pad">&nbsp;</td>', 1 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 2 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 3 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 4 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 5 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 6 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 1 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 2 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 3 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 4 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 5 ),
			str_repeat( '<td class="pad">&nbsp;</td>', 6 ),
			'<td colspan="3" id="prev" class="pad"><span></span></td>',
			'<td colspan="3" id="next" class="pad"><span></span></td>',
		),
		$out
	);

	return $out;
}

/**
 * Whether or not to show global featured content
 *
 * @return boolean
 */
function bimber_show_global_featured_entries() {
	$bimber_fe_visibility = explode( ',', bimber_get_theme_option( 'featured_entries', 'visibility' ) );

	$bimber_fe_show_on_home        = is_home() && in_array( 'home', $bimber_fe_visibility, true );
	$bimber_fe_show_on_single_post = is_single() && in_array( 'single_post', $bimber_fe_visibility, true );

	return apply_filters( 'bimber_show_global_featured_entries', $bimber_fe_show_on_home || $bimber_fe_show_on_single_post );
}

/**
 * Add body class responsible for boxed|stretched layout
 *
 * @param array $classes Body classes.
 *
 * @return array
 */
function bimber_body_class_global_layout( $classes ) {
	$layout    = bimber_get_theme_option( 'global', 'layout' );
	$classes[] = 'g1-layout-' . $layout;

	return $classes;
}

/**
 * Add some body classes.
 *
 * @param array $classes Body classes.
 *
 * @return array
 */
function bimber_body_class_helpers( $classes ) {
	$classes[] = 'g1-hoverable';

	return $classes;
}

/**
 * Add body class indicating there's the mobile logo available.
 *
 * @param array $classes Body classes.
 *
 * @return array
 */
function bimber_body_class_mobile_logo( $classes ) {
	$logo = bimber_get_small_logo();

	if ( ! empty( $logo ) ) {
		$classes[] = 'g1-has-mobile-logo';
	}

	return $classes;
}

/**
 * Hide sharebar on specific pages.
 *
 * @param bool $bool Whether or not the sharebar is visible.
 *
 * @return bool
 */
function bimber_hide_sharebar( $bool ) {
	if ( ! is_single() ) {
		$bool = false;
	}

	return $bool;
}

/**
 * Inserts spans into category listing
 *
 * @param string $in Markup.
 *
 * @return string
 */
function bimber_insert_cat_count_span( $in ) {
	$out = preg_replace( '/<\/a> \(([0-9]+)\)/', ' <span class="g1-meta">\\1</span></a>', $in );

	return $out;
}

/**
 * Inserts spans into archive listing
 *
 * @param string $in Markup.
 *
 * @return string
 */
function bimber_insert_archive_count_span( $in ) {

	if ( false !== strpos( $in, '<li>' ) ) {
		$out = preg_replace( '/<\/a>&nbsp;\(([0-9]+)\)/', ' <span class="g1-meta">\\1</span></a>', $in );

		return $out;
	}

	return $in;
}

/**
 * Change video shortcode attributes to better match the content width
 *
 * @param string $out Markup.
 * @param array  $pairs Entire list of supported attributes and their defaults.
 * @param array  $atts User defined attributes in shortcode tag.
 * @param string $shortcode Shortcode name.
 *
 * @return mixed
 */
function bimber_wp_video_shortcode_atts( $out, $pairs, $atts ) {
	global $content_width;
	$width  = $out['width'];
	$height = $out['height'];

	$out['width']  = $content_width;
	$out['height'] = round( $content_width * $height / $width );

	return $out;
}

function bimber_shorten_number( $value ) {
	if ( $value > 1000000 ) {
		$value = round( $value / 1000000, 1 ) . 'M';
	} elseif ( $value > 1000 ) {
		$value = round( $value / 1000, 1 ) . 'k';
	}

	return $value;
}




function bimber_show_prefooter() {
	$show = true;

	return apply_filters( 'bimber_show_prefooter', $show );
}

/**
 * Whether or not to show the user community profile link.
 *
 * $param int $user_id User ID.
 * @return bool
 *
 * @since 2.0.4
 */
function bimber_show_user_profile_link( $user_id ) {
	$bool = false;

	if ( bimber_can_use_plugin( 'buddypress/bp-loader.php' ) ) {
		$bool = true;
	}

	return apply_filters( 'bimber_show_user_profile_link', $bool, $user_id );
}

/**
 * Generates color variations based on a single color
 *
 * @param Bimber_Color $color Color.
 *
 * @return array
 */
function bimber_get_color_variations($color ) {
	$result = array();

	if ( ! is_a( $color, 'Bimber_Color' ) ) {
		$color = new Bimber_Color( $color );
	}

	$color_rgb = $color->get_rgb();
	$color_rgb = array_map( 'round', $color_rgb );

	$result['hex'] = $color->get_hex();
	$result['r']   = $color_rgb[0];
	$result['g']   = $color_rgb[1];
	$result['b']   = $color_rgb[2];

	$result['from_hex'] = $color->get_hex();
	$result['from_r']   = $color_rgb[0];
	$result['from_g']   = $color_rgb[1];
	$result['from_b']   = $color_rgb[2];
	$result['to_hex']   = $color->get_hex();
	$result['to_r']     = $color_rgb[0];
	$result['to_g']     = $color_rgb[1];
	$result['to_b']     = $color_rgb[2];

	$border2     = Bimber_Color_Generator::get_tone_color( $color, 20 );
	$border2_rgb = $border2->get_rgb();
	$border2_rgb = array_map( 'round', $border2_rgb );

	$border1 = clone $color;
	$border1->set_lightness( round( ( $border1->get_lightness() + $border2->get_lightness() ) / 2 ) );
	$border1_rgb = $border1->get_rgb();
	$border1_rgb = array_map( 'round', $border1_rgb );

	$result['border2_hex'] = $border2->get_hex();
	$result['border2_r']   = $border2_rgb[0];
	$result['border2_g']   = $border2_rgb[1];
	$result['border2_b']   = $border2_rgb[2];

	$result['border1_hex'] = $border1->get_hex();
	$result['border1_r']   = $border1_rgb[0];
	$result['border1_g']   = $border1_rgb[1];
	$result['border1_b']   = $border1_rgb[2];

	if ( $color->get_lightness() >= 50 ) {
		$result['border1_start'] = 0;
		$result['border1_end']   = 0.66;
	} else {
		$result['border1_start'] = 0.66;
		$result['border1_end']   = 0;
	}

	$tone_20_20     = Bimber_Color_Generator::get_tone_color( $color, 20, 20 );
	$tone_20_20_rgb = $tone_20_20->get_rgb();
	$tone_20_20_rgb = array_map( 'round', $tone_20_20_rgb );

	$result['tone_20_20_hex'] = $tone_20_20->get_hex();
	$result['tone_20_20_r']   = $tone_20_20_rgb[0];
	$result['tone_20_20_g']   = $tone_20_20_rgb[1];
	$result['tone_20_20_b']   = $tone_20_20_rgb[2];

	$tone_5_90     = Bimber_Color_Generator::get_tone_color( $color, 5, 90 );
	$tone_5_90_rgb = $tone_5_90->get_rgb();
	$tone_5_90_rgb = array_map( 'round', $tone_5_90_rgb );

	$result['tone_5_90_hex'] = $tone_5_90->get_hex();
	$result['tone_5_90_r']   = $tone_5_90_rgb[0];
	$result['tone_5_90_g']   = $tone_5_90_rgb[1];
	$result['tone_5_90_b']   = $tone_5_90_rgb[2];

	return $result;
}
