<?php
/**
 * Front hooks
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}
// Body classes.
add_filter( 'body_class', 'bimber_body_class_global_layout' );
add_filter( 'body_class', 'bimber_body_class_helpers' );
add_filter( 'body_class', 'bimber_body_class_mobile_logo' );

// Post.
add_filter( 'single_template', 'bimber_post_alter_single_template' );
add_filter( 'post_class', 'bimber_post_class', 20 );

// Home.
add_filter( 'home_template', 'bimber_home_alter_template' );
add_action( 'bimber_home_loop_after_post', 'bimber_home_inject_newsletter_into_loop', 10, 2 );
add_filter( 'bimber_home_loop_after_post', 'bimber_home_inject_ad_into_loop', 10, 2 );
add_action( 'pre_get_posts', 'bimber_home_set_posts_per_page' );
add_action( 'pre_get_posts', 'bimber_home_exclude_featured' );
add_filter( 'found_posts', 'bimber_home_adjust_offset_pagination', 1, 2 );

// Archive.
add_action( 'bimber_archive_loop_after_post', 'bimber_archive_inject_newsletter_into_loop', 10, 2 );
add_filter( 'bimber_archive_loop_after_post', 'bimber_archive_inject_ad_into_loop', 10, 2 );
add_action( 'pre_get_posts', 'bimber_archive_set_posts_per_page' );
add_action( 'pre_get_posts', 'bimber_archive_exclude_featured' );
add_filter( 'found_posts', 'bimber_archive_adjust_offset_pagination', 1, 2 );


// Page.
add_filter( 'wp_link_pages_args', 'bimber_filter_wp_link_pages_args' );
add_filter( 'wp_link_pages_link', 'bimber_filter_wp_link_pages_link', 10, 2 );

// Hot/Popular/Trending.
add_filter( 'the_content', 'bimber_list_hot_entries', 11 );
add_filter( 'the_content', 'bimber_list_popular_entries', 11 );
add_filter( 'the_content', 'bimber_list_trending_entries', 11 );

// Comments.
add_filter( 'comment_form_default_fields', 'bimber_comment_form_default_fields' );
add_filter( 'comment_form_field_comment', 'bimber_comment_form_field_comment' );
add_action( 'comment_form_top', 'bimber_comment_render_avatar_before_form' );
add_filter( 'show_recent_comments_widget_style', '__return_false' );

// Enqueue assets.
add_action( 'wp_head', 'bimber_internal_dynamic_styles' );
add_action( 'wp_head', 'bimber_render_head_scripts', 5 );
add_action( 'wp_enqueue_scripts', 'bimber_enqueue_head_styles' );
add_action( 'wp_enqueue_scripts', 'bimber_enqueue_front_scripts' );
add_filter( 'script_loader_tag', 'bimber_load_front_scripts_conditionally', 10, 2 );

// Meta tags.
add_action( 'wp_head', 'bimber_add_responsive_design_meta_tag', 1 );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

// Other.
add_filter( 'wp_list_categories', 'bimber_insert_cat_count_span' );
add_filter( 'get_archives_link', 'bimber_insert_archive_count_span' );
add_filter( 'get_calendar', 'bimber_alter_calendar_output' );
add_filter( 'bimber_show_sharebar', 'bimber_hide_sharebar' );

// Shortcodes.
add_filter( 'shortcode_atts_video', 'bimber_wp_video_shortcode_atts', 10, 3 );

// Dynamic style cache.
add_action( 'template_redirect', 'bimber_load_dynamic_styles' );
