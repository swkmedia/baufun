<?php
/**
 * WP Customizer panel section to handle post single options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

$wp_customize->add_section( 'bimber_posts_single_section', array(
	'title'    => esc_html__( 'Single', 'bimber' ),
	'priority' => 20,
	'panel'    => 'bimber_posts_panel',
) );

// Template.
$wp_customize->add_setting( $bimber_option_name . '[post_template]', array(
	'default'           => $bimber_customizer_defaults['post_template'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_post_template', array(
	'label'    => esc_html__( 'Template', 'bimber' ),
	'section'  => 'bimber_posts_single_section',
	'settings' => $bimber_option_name . '[post_template]',
	'type'     => 'select',
	'choices'  => array(
		'classic' => esc_html__( 'Classic, no sidebar', 'bimber' ),
		'sidebar' => esc_html__( 'Classic, with sidebar', 'bimber' ),
	),
) );


// Hide Elements.
$wp_customize->add_setting( $bimber_option_name . '[post_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['post_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_post_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'bimber_posts_single_section',
	'settings' => $bimber_option_name . '[post_hide_elements]',
	'choices'  => array(
		'categories'      => esc_html__( 'Categories', 'bimber' ),
		'author'          => esc_html__( 'Author', 'bimber' ),
		'avatar'          => esc_html__( 'Avatar', 'bimber' ),
		'date'            => esc_html__( 'Date', 'bimber' ),
		'comments_link'   => esc_html__( 'Comments Link', 'bimber' ),
		'views'           => esc_html__( 'Views', 'bimber' ),
		'featured_media'  => esc_html__( 'Featured Media', 'bimber' ),
		'tags'            => esc_html__( 'Tags', 'bimber' ),
		'newsletter'      => esc_html__( 'Newsletter', 'bimber' ),
		'navigation'      => esc_html__( 'Prev/Next Links', 'bimber' ),
		'author_info'     => esc_html__( 'Author info', 'bimber' ),
		'related_entries' => esc_html__( 'You May Also Like', 'bimber' ),
		'more_from'       => esc_html__( 'More from category', 'bimber' ),
		'dont_miss'       => esc_html__( 'Don\'t miss', 'bimber' ),
		'comments'        => esc_html__( 'Comments', 'bimber' ),
	),
) ) );


// ShareBar.
$wp_customize->add_setting( $bimber_option_name . '[post_sharebar]', array(
	'default'           => $bimber_customizer_defaults['post_sharebar'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_post_sharebar', array(
	'label'       => esc_html__( 'Sticky ShareBar', 'bimber' ),
	'section'     => 'bimber_posts_single_section',
	'settings'    => $bimber_option_name . '[post_sharebar]',
	'type'        => 'select',
	'choices'     => array(
		'none'     => esc_html__( 'disabled', 'bimber' ),
		'standard' => esc_html__( 'enabled', 'bimber' ),
	),
) );



// Pagination.
$wp_customize->add_setting( 'bimber_post_pagination_header', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_post_pagination_header', array(
	'section'  => 'bimber_posts_single_section',
	'settings' => 'bimber_post_pagination_header',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'Pagination', 'bimber' ) . '</h2>',
) ) );

// Pagination: overview
$wp_customize->add_setting( $bimber_option_name . '[post_pagination_overview]', array(
	'default'           => $bimber_customizer_defaults['post_pagination_overview'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_post_pagination_overview', array(
	'label'       => esc_html__( 'Overview', 'bimber' ),
	'section'     => 'bimber_posts_single_section',
	'settings'    => $bimber_option_name . '[post_pagination_overview]',
	'type'        => 'select',
	'choices'     => array(
		'page_links'        => esc_html__( 'page links', 'bimber' ),
		'page_xofy'         => esc_html__( 'page X of Y', 'bimber' ),
		'none'              => esc_html__( 'none', 'bimber' ),
	),
) );

// Pagination: adjacent label
$wp_customize->add_setting( $bimber_option_name . '[post_pagination_adjacent_label]', array(
	'default'           => $bimber_customizer_defaults['post_pagination_adjacent_label'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_post_pagination_adjacent_label', array(
	'label'       => esc_html__( 'Adjacent label', 'bimber' ),
	'section'     => 'bimber_posts_single_section',
	'settings'    => $bimber_option_name . '[post_pagination_adjacent_label]',
	'type'        => 'select',
	'choices'     => array(
		'adjacent'      => esc_html__( 'previous | next', 'bimber' ),
		'adjacent_page' => esc_html__( 'previous page | next page', 'bimber' ),
		'arrow'         => esc_html__( 'just arrow', 'bimber' ),
	),
) );

// Pagination: adjacent style
$wp_customize->add_setting( $bimber_option_name . '[post_pagination_adjacent_style]', array(
	'default'           => $bimber_customizer_defaults['post_pagination_adjacent_style'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_post_pagination_adjacent_style', array(
	'label'       => esc_html__( 'Adjacent style', 'bimber' ),
	'section'     => 'bimber_posts_single_section',
	'settings'    => $bimber_option_name . '[post_pagination_adjacent_style]',
	'type'        => 'select',
	'choices'     => array(
		'link'      => esc_html__( 'link', 'bimber' ),
		'button'    => esc_html__( 'button', 'bimber' ),
	),
) );



// You May Also Like section header.
$wp_customize->add_setting( 'bimber_post_related_header', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_post_related_header', array(
	'section'  => 'bimber_posts_single_section',
	'settings' => 'bimber_post_related_header',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'You May Also Like', 'bimber' ) . '</h2>',
	'active_callback' => 'bimber_customizer_is_related_active',
) ) );

// You May Also Like section.
$wp_customize->add_setting( $bimber_option_name . '[post_related_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['post_related_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_post_related_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'bimber_posts_single_section',
	'settings' => $bimber_option_name . '[post_related_hide_elements]',
	'choices'  => array(
		'featured_media'  => esc_html__( 'Featured Media', 'bimber' ),
		'shares'          => esc_html__( 'Shares', 'bimber' ),
		'views'           => esc_html__( 'Views', 'bimber' ),
		'comments_link'   => esc_html__( 'Comments Link', 'bimber' ),
		'categories'      => esc_html__( 'Categories', 'bimber' ),
		'summary'         => esc_html__( 'Summary', 'bimber' ),
		'author'          => esc_html__( 'Author', 'bimber' ),
		'avatar'          => esc_html__( 'Avatar', 'bimber' ),
		'date'            => esc_html__( 'Date', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_related_active',
) ) );

/**
 * Check whether user hide the You May Also Like section
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_related_active( $control ) {
	$hidden_elements = $control->manager->get_setting( bimber_get_theme_id() . '[post_hide_elements]' )->value();

	return false === strpos( $hidden_elements, 'related_entries' );
}

// More From section header.
$wp_customize->add_setting( 'bimber_post_more_from_header', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_post_more_from_header', array(
	'section'  => 'bimber_posts_single_section',
	'settings' => 'bimber_post_more_from_header',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'More From', 'bimber' ) . '</h2>',
	'active_callback' => 'bimber_customizer_is_more_from_active',
) ) );

// More From section.
$wp_customize->add_setting( $bimber_option_name . '[post_more_from_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['post_more_from_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_post_more_from_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'bimber_posts_single_section',
	'settings' => $bimber_option_name . '[post_more_from_hide_elements]',
	'choices'  => array(
		'featured_media'  => esc_html__( 'Featured Media', 'bimber' ),
		'shares'          => esc_html__( 'Shares', 'bimber' ),
		'views'           => esc_html__( 'Views', 'bimber' ),
		'comments_link'   => esc_html__( 'Comments Link', 'bimber' ),
		'categories'      => esc_html__( 'Categories', 'bimber' ),
		'summary'         => esc_html__( 'Summary', 'bimber' ),
		'author'          => esc_html__( 'Author', 'bimber' ),
		'avatar'          => esc_html__( 'Avatar', 'bimber' ),
		'date'            => esc_html__( 'Date', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_more_from_active',
) ) );


/**
 * Check whether user hide the More From section
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_more_from_active( $control ) {
	$hidden_elements = $control->manager->get_setting( bimber_get_theme_id() . '[post_hide_elements]' )->value();

	return false === strpos( $hidden_elements, 'more_from' );
}

// Don't Miss section header.
$wp_customize->add_setting( 'bimber_post_dont_miss_hide_elements_header', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_post_dont_miss_hide_elements_header', array(
	'section'  => 'bimber_posts_single_section',
	'settings' => 'bimber_post_dont_miss_hide_elements_header',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'Don\'t Miss', 'bimber' ) . '</h2>',
	'active_callback' => 'bimber_customizer_is_dont_miss_active',
) ) );

// Don't Miss section.
$wp_customize->add_setting( $bimber_option_name . '[post_dont_miss_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['post_dont_miss_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_post_dont_miss_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'bimber_posts_single_section',
	'settings' => $bimber_option_name . '[post_dont_miss_hide_elements]',
	'choices'  => array(
		'featured_media'  => esc_html__( 'Featured Media', 'bimber' ),
		'shares'          => esc_html__( 'Shares', 'bimber' ),
		'views'           => esc_html__( 'Views', 'bimber' ),
		'comments_link'   => esc_html__( 'Comments Link', 'bimber' ),
		'categories'      => esc_html__( 'Categories', 'bimber' ),
		'summary'         => esc_html__( 'Summary', 'bimber' ),
		'author'          => esc_html__( 'Author', 'bimber' ),
		'avatar'          => esc_html__( 'Avatar', 'bimber' ),
		'date'            => esc_html__( 'Date', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_dont_miss_active',
) ) );

/**
 * Check whether user hide the Don't Miss section
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_dont_miss_active( $control ) {
	$hidden_elements = $control->manager->get_setting( bimber_get_theme_id() . '[post_hide_elements]' )->value();

	return false === strpos( $hidden_elements, 'dont_miss' );
}