<?php
/**
 * WP Customizer panel section to handle general design options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

$wp_customize->add_section( 'bimber_design_global_section', array(
	'title'    => esc_html__( 'Global', 'bimber' ),
	'priority' => 10,
	'panel'    => 'bimber_design_panel',
) );

// Page layout.
$wp_customize->add_setting( $bimber_option_name . '[global_layout]', array(
	'default'           => $bimber_customizer_defaults['global_layout'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_global_layout', array(
	'label'    => esc_html__( 'Layout', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[global_layout]',
	'type'     => 'select',
	'choices'  => array(
		'boxed'     => esc_html__( 'boxed', 'bimber' ),
		'stretched' => esc_html__( 'stretched', 'bimber' ),
	),
) );

// Page width.
$wp_customize->add_setting( $bimber_option_name . '[global_width]', array(
	'default'           => $bimber_customizer_defaults['global_width'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'absint',
) );

$wp_customize->add_control( 'bimber_global_width', array(
	'label'    => esc_html__( 'Boxed Layout Width', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[global_width]',
	'type'     => 'text',
) );

// Background Color.
$wp_customize->add_setting( $bimber_option_name . '[global_background_color]', array(
	'default'           => $bimber_customizer_defaults['global_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_global_background_color', array(
	'label'    => esc_html__( 'Boxed Layout Background', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[global_background_color]',
) ) );


// Google Font Subset.
$wp_customize->add_setting( $bimber_option_name . '[global_google_font_subset]', array(
	'default'           => $bimber_customizer_defaults['global_google_font_subset'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_global_google_font_subset', array(
	'label'    => esc_html__( 'Google Font Subset', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[global_google_font_subset]',
	'choices'  => array(
		'latin'        => esc_html__( 'Latin', 'bimber' ),
		'latin-ext'    => esc_html__( 'Latin Extended', 'bimber' ),
		'cyrillic'     => esc_html__( 'Cyrillic', 'bimber' ),
		'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'bimber' ),
		'greek'        => esc_html__( 'Greek', 'bimber' ),
		'greek-ext'    => esc_html__( 'Greek Extended', 'bimber' ),
		'vietnamese'   => esc_html__( 'Vietnamese', 'bimber' ),
	),
) ) );

// Divider.
$wp_customize->add_setting( 'bimber_global_cs_1_divider', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_global_cs_1_divider', array(
	'section'  => 'bimber_design_global_section',
	'settings' => 'bimber_global_cs_1_divider',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'Basic Color Scheme', 'bimber' ) . '</h2>
		<p>' . esc_html__( 'Will be applied to buttons, badges.', 'bimber' ) . '</p>',
) ) );


// Background Color (cs1).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_1_background_color]', array(
	'default'           => $bimber_customizer_defaults['content_cs_1_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_1_background_color', array(
	'label'    => esc_html__( 'Background', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_1_background_color]',
) ) );


// Text 1 (cs1).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_1_text1]', array(
	'default'           => $bimber_customizer_defaults['content_cs_1_text1'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_1_text1', array(
	'label'    => esc_html__( 'Headings &amp; Titles', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_1_text1]',
) ) );


// Text 2 (cs1).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_1_text2]', array(
	'default'           => $bimber_customizer_defaults['content_cs_1_text2'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_1_text2', array(
	'label'    => esc_html__( 'Regular Text', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_1_text2]',
) ) );


// Text 3 (cs1).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_1_text3]', array(
	'default'           => $bimber_customizer_defaults['content_cs_1_text3'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_1_text3', array(
	'label'       => esc_html__( 'Small Text Descriptions', 'bimber' ),
	'description' => esc_html__( 'Post bylines, comment dates, meta information', 'bimber' ),
	'section'     => 'bimber_design_global_section',
	'settings'    => $bimber_option_name . '[content_cs_1_text3]',
) ) );


// Accent 1 (cs1).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_1_accent1]', array(
	'default'           => $bimber_customizer_defaults['content_cs_1_accent1'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_1_accent1', array(
	'label'    => esc_html__( 'Accent', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_1_accent1]',
) ) );


// Divider.
$wp_customize->add_setting( 'bimber_global_cs_2_divider', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_global_cs_2_divider', array(
	'section'  => 'bimber_design_global_section',
	'settings' => 'bimber_global_cs_2_divider',
	'html'     =>
		'<hr />
		<h2>' . esc_html__( 'Secondary Color Scheme', 'bimber' ) . '</h2>
		<p>' . esc_html__( 'Will be applied to buttons, badges &amp; flags', 'bimber' ) . '</p>',
) ) );


// Background Color (cs2).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_2_background_color]', array(
	'default'           => $bimber_customizer_defaults['content_cs_2_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_2_background_color', array(
	'label'    => esc_html__( 'Background', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_2_background_color]',
) ) );


// Text 1 (cs2).
$wp_customize->add_setting( $bimber_option_name . '[content_cs_2_text1]', array(
	'default'           => $bimber_customizer_defaults['content_cs_2_text1'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );

$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_content_cs_2_text1', array(
	'label'    => esc_html__( 'Text', 'bimber' ),
	'section'  => 'bimber_design_global_section',
	'settings' => $bimber_option_name . '[content_cs_2_text1]',
) ) );


