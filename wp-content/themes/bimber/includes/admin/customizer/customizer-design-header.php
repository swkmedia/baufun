<?php
/**
 * WP Customizer panel section to handle header design options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

$wp_customize->add_section( 'bimber_design_header_section', array(
	'title'    => esc_html__( 'Header', 'bimber' ),
	'priority' => 40,
	'panel'    => 'bimber_design_panel',
) );

// Logo Margin Top.
$wp_customize->add_setting( $bimber_option_name . '[header_logo_margin_top]', array(
	'default'           => $bimber_customizer_defaults['header_logo_margin_top'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( 'bimber_header_logo_margin_top', array(
	'label'    => esc_html__( 'Logo Margin Top', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_logo_margin_top]',
	'type'     => 'number',
) );

// Logo Margin Bottom.
$wp_customize->add_setting( $bimber_option_name . '[header_logo_margin_bottom]', array(
	'default'           => $bimber_customizer_defaults['header_logo_margin_bottom'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_header_logo_margin_bottom', array(
	'label'    => esc_html__( 'Logo Margin Bottom', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_logo_margin_bottom]',
	'type'     => 'number',
) );


// Quick Nav Margin Top.
$wp_customize->add_setting( $bimber_option_name . '[header_quicknav_margin_top]', array(
	'default'           => $bimber_customizer_defaults['header_quicknav_margin_top'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( 'bimber_header_quicknav_margin_top', array(
	'label'    => esc_html__( 'Quick Nav Margin Top', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_quicknav_margin_top]',
	'type'     => 'number',
) );

// Quick Nav Margin Bottom.
$wp_customize->add_setting( $bimber_option_name . '[header_quicknav_margin_bottom]', array(
	'default'           => $bimber_customizer_defaults['header_quicknav_margin_bottom'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );
$wp_customize->add_control( 'bimber_header_quicknav_margin_bottom', array(
	'label'    => esc_html__( 'Quick Nav Margin Bottom', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_quicknav_margin_bottom]',
	'type'     => 'number',
) );


// Text Color.
$wp_customize->add_setting( $bimber_option_name . '[header_text_color]', array(
	'default'           => $bimber_customizer_defaults['header_text_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_text_color', array(
	'label'    => esc_html__( 'Text', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_text_color]',
) ) );

// Accent Color.
$wp_customize->add_setting( $bimber_option_name . '[header_accent_color]', array(
	'default'           => $bimber_customizer_defaults['header_accent_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_accent_color', array(
	'label'    => esc_html__( 'Accent', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_accent_color]',
) ) );

// Background Color.
$wp_customize->add_setting( $bimber_option_name . '[header_background_color]', array(
	'default'           => $bimber_customizer_defaults['header_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_background_color', array(
	'label'    => esc_html__( 'Background', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_background_color]',
) ) );


// Navbar Background Color.
$wp_customize->add_setting( $bimber_option_name . '[header_navbar_background_color]', array(
	'default'           => $bimber_customizer_defaults['header_navbar_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_navbar_background_color', array(
	'label'    => esc_html__( 'Navbar Background', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_navbar_background_color]',
) ) );

// Navbar Text Color.
$wp_customize->add_setting( $bimber_option_name . '[header_navbar_text_color]', array(
	'default'           => $bimber_customizer_defaults['header_navbar_text_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_navbar_text_color', array(
	'label'    => esc_html__( 'Navbar Text', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_navbar_text_color]',
) ) );

// Navbar Accent Color.
$wp_customize->add_setting( $bimber_option_name . '[header_navbar_accent_color]', array(
	'default'           => $bimber_customizer_defaults['header_navbar_accent_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_navbar_accent_color', array(
	'label'    => esc_html__( 'Navbar Accent', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_navbar_accent_color]',
) ) );

// Submenu Background Color.
$wp_customize->add_setting( $bimber_option_name . '[header_submenu_background_color]', array(
	'default'           => $bimber_customizer_defaults['header_submenu_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_submenu_background_color', array(
	'label'    => esc_html__( 'Navbar Submenu Background', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_submenu_background_color]',
) ) );

// Submenu Text Color.
$wp_customize->add_setting( $bimber_option_name . '[header_submenu_text_color]', array(
	'default'           => $bimber_customizer_defaults['header_submenu_text_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_submenu_text_color', array(
	'label'    => esc_html__( 'Navbar Submenu Text', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_submenu_text_color]',
) ) );

// Submenu Accent Color.
$wp_customize->add_setting( $bimber_option_name . '[header_submenu_accent_color]', array(
	'default'           => $bimber_customizer_defaults['header_submenu_accent_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_submenu_accent_color', array(
	'label'    => esc_html__( 'Navbar Submenu Accent', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_submenu_accent_color]',
) ) );



// Navbar Secondary Background Color.
$wp_customize->add_setting( $bimber_option_name . '[header_navbar_secondary_background_color]', array(
	'default'           => $bimber_customizer_defaults['header_navbar_secondary_background_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_navbar_secondary_background_color', array(
	'label'    => esc_html__( 'Button Background', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_navbar_secondary_background_color]',
) ) );

// Navbar Secondary Text Color.
$wp_customize->add_setting( $bimber_option_name . '[header_navbar_secondary_text_color]', array(
	'default'           => $bimber_customizer_defaults['header_navbar_secondary_text_color'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_hex_color',
) );
$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bimber_header_navbar_secondary_text_color', array(
	'label'    => esc_html__( 'Button Text', 'bimber' ),
	'section'  => 'bimber_design_header_section',
	'settings' => $bimber_option_name . '[header_navbar_secondary_text_color]',
) ) );
