<?php
/**
 * Class for setting up demo data
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

if ( ! class_exists( 'Bimber_Demo_Data' ) ) {
	/**
	 * Class Bimber_Demo_Data
	 */
	class Bimber_Demo_Data {

		/**
		 * Import theme content, widgets and assigns menus
		 *
		 * @return array        Response: status, message
		 */
		public function import_content() {

			require_once BIMBER_ADMIN_DIR . 'lib/class-bimber-import-export.php';

			$content_path = trailingslashit( get_template_directory() ) . 'dummy-data/dummy-data.xml.gz';

			$importer_out = Bimber_Import_Export::import_content_from_file( $content_path );

			// Demo content imported successfully?
			if ( null !== $importer_out ) {
				$response = array(
					'status'  => 'success',
					'message' => $importer_out,
				);

				// Set up menus.
				$this->assign_menus();

				// Import widgets.
				$widgets_path = trailingslashit( get_template_directory() ) . 'dummy-data/widgets.txt';

				Bimber_Import_Export::import_widgets_from_file( $widgets_path );

				do_action( 'bimber_after_import_content' );
			} else {
				$response = array(
					'status'  => 'error',
					'message' => esc_html__( 'Failed to import content', 'bimber' ),
				);
			}

			return $response;
		}

		/**
		 * Import theme options
		 *
		 * @return array            Response status and message
		 */
		public function import_theme_options() {

			require_once BIMBER_ADMIN_DIR . 'lib/class-bimber-import-export.php';

			$demo_options_path = trailingslashit( get_template_directory() ) . 'dummy-data/theme-options.txt';

			if ( Bimber_Import_Export::import_options_from_file( $demo_options_path ) ) {
				$response = array(
					'status'  => 'success',
					'message' => esc_html__( 'Theme options imported successfully.', 'bimber' ),
				);
			} else {
				$response = array(
					'status'  => 'error',
					'message' => esc_html__( 'Failed to import theme options', 'bimber' ),
				);
			}

			return $response;
		}

		/**
		 * Import theme options and content
		 *
		 * @return array        Response status and message
		 */
		public function import_all() {
			$content_response = $this->import_content();

			if ( 'error' === $content_response['status'] ) {
				return $content_response;
			}

			$theme_options_response = $this->import_theme_options();

			if ( 'error' === $theme_options_response['status'] ) {
				return $theme_options_response;
			}

			// If all goes well.
			$response = array(
				'status'  => 'success',
				'message' => esc_html__( 'Import completed successfully.', 'bimber' ),
			);

			return $response;
		}

		/**
		 * Assign menus to locations
		 */
		protected function assign_menus() {
			$menu_location = array(
				'PrimaryMenu' => 'bimber_primary_nav',
				'FooterMenu'  => 'bimber_footer_nav',
			);

			$registered_locations = get_registered_nav_menus();
			$locations            = get_nav_menu_locations();

			foreach ( $menu_location as $menu => $location ) {
				if ( empty( $menu ) && isset( $locations[ $location ] ) ) {
					unset( $locations[ $location ] );
					continue;
				}

				$menu_obj = wp_get_nav_menu_object( $menu );

				if ( ! $menu_obj || is_wp_error( $menu_obj ) ) {
					printf( esc_html__( 'Invalid menu "%s".', 'bimber' ), esc_html( $menu ) );
					continue;
				}

				if ( ! array_key_exists( $location, $registered_locations ) ) {
					printf( esc_html__( 'Invalid location "%s".', 'bimber' ), esc_html( $location ) );
					continue;
				}

				$locations[ $location ] = $menu_obj->term_id;
			}

			set_theme_mod( 'nav_menu_locations', $locations );
		}
	}
}

