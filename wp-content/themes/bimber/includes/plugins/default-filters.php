<?php
/**
 * Plugin hooks
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

// Wordpress Popular Posts.
if ( bimber_can_use_plugin( 'wordpress-popular-posts/wordpress-popular-posts.php' ) ) {
	add_action( 'widgets_init', 'bimber_wpp_remove_widget' );
	add_filter( 'bimber_most_viewed_query_args', 'bimber_wpp_get_most_viewed_query_args', 10, 2 );
	add_filter( 'bimber_entry_view_count', 'bimber_wpp_get_view_count' );
}

// Mashshare.
add_action( 'after_setup_theme', 'bimber_mashsharer_disable_welcome_redirect' );

// Only core loaded.
if ( bimber_can_use_plugin( 'mashsharer/mashshare.php' ) ) {
	add_filter( 'bimber_most_shared_query_args',    'bimber_mashsharer_get_most_shared_query_args', 10, 2 );
	add_filter( 'bimber_entry_share_count',         'bimber_mashsharer_get_share_count' );
	add_filter( 'bimber_show_entry_share_count',    'bimber_mashsharer_show_share_count', 10, 2 );
	add_action( 'bimber_after_import_content',      'bimber_mashsharer_set_defaults' );

	// Custom caching rules to not refresh counters on archives.
	// Curl requests coast too much, so reload cache only on a single page.
	if ( ! is_admin() ) {
		add_action( 'init',         'bimber_mashsharer_init_custom_caching_rules' );
		add_filter( 'the_content',  'bimber_mashsharer_activate_curl', 1 );
		add_filter( 'the_content',  'bimber_mashsharer_deactivate_curl', 9999 );
	}
}

// Core loaded but not Networks addon.
if ( bimber_can_use_plugin( 'mashsharer/mashshare.php' ) && ! bimber_can_use_plugin( 'mashshare-networks/mashshare-networks.php' ) ) {
	add_filter( 'mashsb_array_networks',    'bimber_mashsharer_array_networks' );
	add_action( 'init',                     'bimber_mashsharer_register_new_networks' );
	add_action( 'plugins_loaded',           'bimber_mashsharer_add_networks_class' );
}

// Core and Networks addon loaded.
if ( bimber_can_use_plugin( 'mashsharer/mashshare.php' ) && bimber_can_use_plugin( 'mashshare-networks/mashshare-networks.php' ) ) {
	add_action( 'init', 'bimber_mashsharer_deregister_new_networks' );
}

// Core and ShareBar addon loaded.
if ( bimber_can_use_plugin( 'mashsharer/mashshare.php' ) && bimber_can_use_plugin( 'mashshare-sharebar/mashshare-sharebar.php' ) ) {
	// Disable our built-in bar.
	add_filter( 'bimber_show_sharebar', '__return_false', 99 );
}

// Mailchimp for WP.
if ( bimber_can_use_plugin( 'mailchimp-for-wp/mailchimp-for-wp.php' ) ) {
	add_filter( 'mc4wp_form_before_fields', 'bimber_mc4wp_form_before_form', 10, 2 );
	add_filter( 'mc4wp_form_after_fields', 'bimber_mc4wp_form_after_form', 10, 2 );
	add_action( 'bimber_after_import_content', 'bimber_mc4wp_set_up_default_form_id' );
}

// WP QUADS - Quick AdSense Reloaded.
add_action( 'after_setup_theme', 'bimber_quads_disable_welcome_redirect' );

if ( bimber_can_use_plugin( 'quick-adsense-reloaded/quick-adsense-reloaded.php' ) ) {
	add_action( 'after_setup_theme', 'bimber_quads_register_ad_locations' );
	add_filter( 'quads_has_ad', 'bimber_quads_hide_ads', 10, 2 );
}

// WooCommerce.
if ( bimber_can_use_plugin( 'woocommerce/woocommerce.php' ) ) {
	add_action( 'after_setup_theme', 'bimber_woocommerce_support' );
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
	add_action( 'woocommerce_before_main_content', 'bimber_woocommerce_content_wrapper_start', 10 );
	add_action( 'woocommerce_after_main_content', 'bimber_woocommerce_content_wrapper_end', 10 );
	add_action( 'woocommerce_sidebar', 'bimber_woocommerce_sidebar_wrapper_end', 10 );
}

// Loco Translate.
if ( bimber_can_use_plugin( 'loco-translate/loco.php' ) ) {
	add_action( 'admin_notices', 'bimber_loco_notices' );
	add_action( 'admin_enqueue_scripts', 'bimber_logo_admin_enqueue_scripts' );
}

// Snax.
add_action( 'snax_setup_theme', 'bimber_snax_setup' );	// On plugin activation.

if ( bimber_can_use_plugin( 'snax/snax.php' ) ) {
	add_action( 'after_switch_theme', 'bimber_snax_setup' ); // On theme activation.

	// It's not optimal way but it's the only one.
	// We can't hook into plugin activation because the hook process performs an instant redirect after it fires.
	// We can use recommended workaround (add_option()) but it's exaclty the same, in case of performance.
	add_action( 'admin_init', 'bimber_snax_setup' ); // On plugin activation.

	add_filter( 'snax_get_collection_item_image_size',  'bimber_snax_get_collection_item_image_size' );
	//add_filter( 'bimber_use_sticky_header',             'bimber_snax_disable_sticky_header' );

	add_filter( 'bimber_show_quick_nav_menu',           'bimber_snax_hide_on_frontend_submission_page' );
	add_filter( 'bimber_show_navbar_searchform',        'bimber_snax_hide_on_frontend_submission_page' );
	add_filter( 'bimber_show_navbar_socials',           'bimber_snax_hide_on_frontend_submission_page' );
	add_filter( 'bimber_show_prefooter',                'bimber_snax_hide_on_frontend_submission_page' );
	add_filter( 'has_nav_menu',                         'bimber_snax_hide_primary_nav_menu', 10, 2 );

	//add_filter( 'quads_has_ad',                         'bimber_snax_hide_ad_before_content_theme_area', 10, 2 );
	//remove_action( 'snax_before_item_media',            'snax_item_render_notes' );
	//add_action( 'bimber_before_content_theme_area',     'snax_item_render_notes' );

	// Embed width.
	//add_action( 'snax_before_card_media',               'snax_embed_change_content_width' );
	//add_action( 'snax_after_card_media',                'snax_embed_revert_content_width' );


	add_filter( 'snax_capture_item_position_args',      'bimber_snax_capture_item_position_args' );
	add_filter( 'snax_widget_cta_options',              'bimber_snax_widget_cta_options' );
	add_action( 'snax_before_widget_cta_title',         'bimber_snax_before_widget_cta_title' );
}


// BuddyPress.
if ( bimber_can_use_plugin( 'buddypress/bp-loader.php' ) ) {
	add_filter( 'snax_bp_component_main_nav',   'bimber_snax_bp_component_main_nav', 999, 2 );

	add_action( 'bp_member_header_actions', 'bimber_bp_member_add_button_class_filters', 1 );
	add_action( 'bp_member_header_actions', 'bimber_bp_member_remove_button_class_filters', 9999 );

	add_action( 'bp_group_header_actions', 'bimber_bp_group_add_button_class_filters', 1 );
	add_action( 'bp_group_header_actions', 'bimber_bp_group_remove_button_class_filters', 9999 );

	add_filter( 'bp_get_add_friend_button', 			'bimber_bp_get_simple_xs_button', 99 );
	add_filter( 'bp_get_group_join_button', 			'bimber_bp_get_simple_xs_button', 99 );
	add_filter( 'bp_get_send_public_message_button', 	'bimber_bp_get_simple_xs_button', 99 );
	add_filter( 'bp_get_send_message_button_args', 		'bimber_bp_get_simple_xs_button', 99 );

	add_filter( 'bp_before_xprofile_cover_image_settings_parse_args', 'bimber_cover_image_css', 10, 1 );
	add_filter( 'bp_before_groups_cover_image_settings_parse_args', 'bimber_cover_image_css', 10, 1 );

	//add_action ( 'snax_template_after_list_items_loop', 'bimber_render_markup_after_list_items_loop' );

	//add_action ( 'snax_template_before_list_items_loop', 'bimber_render_markup_before_list_items_loop' );

	add_filter( 'bp_directory_members_search_form', 'bimber_bp_directory_search_form' );
	add_filter( 'bp_directory_groups_search_form', 'bimber_bp_directory_search_form' );

	add_filter( 'author_link', 'bimber_bp_get_author_link', 10, 3 );
}

// Auto Load Next Post.
if ( bimber_can_use_plugin( 'auto-load-next-post/auto-load-next-post.php' ) ) {
	// Disable plugin's partial location function that doesn't support child themes.
	remove_action( 'template_redirect', 'auto_load_next_post_template_redirect' );

	// Use custom function with child theme support (till plugin doesn't fix it).
	add_action( 'template_redirect', 'bimber_auto_load_next_post_template_redirect' );

	add_filter( 'auto_load_next_post_general_settings', 'bimber_auto_load_next_post_general_settings' );

	// Return values valid for the theme.
	add_filter( 'pre_option_auto_load_next_post_content_container', 	'bimber_auto_load_next_post_content_container' );
	add_filter( 'pre_option_auto_load_next_post_title_selector', 		'bimber_auto_load_next_post_title_selector' );
	add_filter( 'pre_option_auto_load_next_post_navigation_container', 	'bimber_auto_load_next_post_navigation_container' );
	add_filter( 'pre_option_auto_load_next_post_comments_container',	'bimber_auto_load_next_post_comments_container' );
}

// Facebook Comments.
if ( bimber_can_use_plugin( 'facebook-comments-plugin/facebook-comments.php' ) ) {
	// Disable plugin default location.
	remove_filter( 'the_content', 'fbcommentbox', 100 );

	// Override comments template with FB comments.
	add_filter( 'comments_template', 'bimber_fb_comments_template' );
}
