<?php
/**
 * Snax plugin functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Adjust theme for Snax
 */
function bimber_snax_setup() {
	if ( get_option( 'snax_setup_done', false ) ) {
		return;
	}

	// Change Frontend Submission page template.
	$front_page_id 	= snax_get_frontend_submission_page_id();

	if ( $front_page_id ) {
		update_post_meta( $front_page_id, '_wp_page_template', 'g1-template-page-full.php' );
	}

	update_option( 'snax_setup_done', true );
}

/**
 * Adjust the image size used inside snax collection
 *
 * @param string $image_size Image size.
 *
 * @return string
 */
function bimber_snax_get_collection_item_image_size($image_size ) {
	if ( has_image_size( 'bimber-grid-fancy' ) ) {
		$image_size = 'bimber-grid-fancy';
	}

	return $image_size;
}

/**
 * Disable sticky sharebar on the Frontend Submission page
 *
 * @param bool $bool Whether or not to use the sticky header.
 * @return bool
 */
function bimber_snax_disable_sticky_header($bool ) {
	if ( is_page( snax_get_frontend_submission_page_id() ) ) {
		$bool = false;
	}

	return $bool;
}

/**
 * Hide the prefooter on the frontend submission page
 *
 * @param bool $show Whether or not to show the prefooter.
 *
 * @return bool
 */
function bimber_snax_hide_prefooter( $show ) {
	if ( is_page( snax_get_frontend_submission_page_id() ) ) {
		$show = false;
	}

	return $show;
}

/**
 * Hide the primary nav menu on the frontend submission page
 *
 * @param bool   $has_nav_menu Whether or not a menu is assigned to nav location.
 * @param string $location Nav location.
 *
 * @return bool
 */
function bimber_snax_hide_primary_nav_menu( $has_nav_menu, $location ) {
	if ( 'bimber_primary_nav' === $location && is_page( snax_get_frontend_submission_page_id() ) ) {
		$has_nav_menu = false;
	}

	return $has_nav_menu;
}


/**
 * Hide ad before the content theme area, after snax item submission
 *
 * @param bool   $bool Whether or not an ad is assigned to ad location.
 * @param string $location Ad location.
 *
 * @return bool
 */
function bimber_snax_hide_ad_before_content_theme_area($bool, $location ) {
	if ( 'bimber_before_content_theme_area' === $location && snax_item_submitted() ) {
		$bool = false;
	}

	return $bool;
}

function snax_embed_change_content_width() {
	global $content_width;
	global $snax_old_content_width;

	// Store original value.
	$snax_old_content_width = $content_width;

	// Overide.
	$content_width = 758;
}

function snax_embed_revert_content_width() {
	global $content_width;
	global $snax_old_content_width;

	// Restore.
	$content_width = $snax_old_content_width;
}



/**
 * Hide an element on the frontend submission page
 *
 * @param bool $show Whether or not to show an element.
 *
 * @return bool
 */
function bimber_snax_hide_on_frontend_submission_page( $show ) {
	if ( is_page( snax_get_frontend_submission_page_id() ) ) {
		$show = false;
	}

	return $show;
}



function bimber_snax_capture_item_position_args( $args ) {
	$args['prefix'] = '#';
	$args['suffix'] = ' ';

	return $args;
}

function bimber_snax_widget_cta_options( $args ) {
	$args['classname'] .= ' g1-box g1-box-centered';

	return $args;
}

function bimber_snax_before_widget_cta_title() {
	echo '<i class="g1-box-icon"></i>';
}
