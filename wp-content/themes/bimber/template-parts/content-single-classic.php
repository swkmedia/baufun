<?php
/**
 * The default template for displaying single post content (with sidebar).
 * This is a template part. It must be used within The Loop.
 *
 * @package Bimber_Theme
 */

$bimber_entry_data = bimber_get_template_part_data();
$bimber_elements   = $bimber_entry_data['elements'];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry-tpl-classic' ); ?> itemscope="" itemtype="<?php echo esc_attr( bimber_get_entry_microdata_itemtype() ); ?>">

	<header class="entry-header">
		<?php
		if ( $bimber_elements['categories'] ) :
			bimber_render_entry_categories( array(
				'class'         => 'entry-categories-solid',
				'use_microdata' => true,
			) );
		endif;
		?>

		<?php the_title( '<h1 class="g1-mega g1-mega-1st entry-title" itemprop="headline">', '</h1>' ); ?>

		<?php
		if ( bimber_can_use_plugin( 'wp-subtitle/wp-subtitle.php' ) ) :
			the_subtitle( '<h2 class="entry-subtitle g1-gamma g1-gamma-3rd" itemprop="description">', '</h2>' );
		endif;
		?>

		<?php if ( $bimber_elements['author'] || $bimber_elements['date'] || $bimber_elements['views'] || $bimber_elements['comments_link'] ) : ?>
			<p class="entry-meta entry-meta-m">
				<span class="entry-meta-wrap">
						<?php
						if ( $bimber_elements['author'] ) :
							bimber_render_entry_author( array(
								'avatar'        => $bimber_elements['avatar'],
								'avatar_size'   => 40,
								'use_microdata' => true,
							) );
						endif;
						?>

						<?php
						if ( $bimber_elements['date'] ) :
							bimber_render_entry_date( array( 'use_microdata' => true ) );
						endif;
						?>
				</span>

				<span class="entry-meta-wrap">

					<?php
					if ( $bimber_elements['views'] ) :
					    bimber_render_entry_view_count();
					endif;
					?>

					<?php
					if ( $bimber_elements['comments_link'] ) :
					    bimber_render_entry_comments_link();
					endif;
					?>
				</span>
			</p>
		<?php endif; ?>

	</header>

	<?php
	if ( bimber_show_entry_featured_media( $bimber_elements['featured_media'] ) ) :
		bimber_render_entry_featured_media( array(
			'size'          => 'bimber-grid-2of3',
			'class'         => 'entry-featured-media-main',
			'use_microdata' => true,
			'apply_link'    => false,
		) );
	endif;
	?>

	<div class="entry-content g1-typography-xl" itemprop="articleBody">
		<?php
		the_content();
		wp_link_pages();
		?>
	</div>

	<div class="entry-after">
		<?php
		if ( $bimber_elements['tags'] ) :
			bimber_render_entry_tags();
		endif;
		?>

		<?php
		if ( $bimber_elements['newsletter'] ) :
			get_template_part( 'template-parts/newsletter-after-content' );
		endif;
		?>

		<?php
		if ( $bimber_elements['navigation'] ) :
			get_template_part( 'template-parts/nav-single' );
		endif;
		?>

		<?php
		if ( $bimber_elements['author_info'] ) :
			get_template_part( 'template-parts/author-info' );
		endif;
		?>
	</div>

	<?php get_template_part( 'template-parts/ad-before-related-entries' ); ?>

	<?php
	if ( $bimber_elements['related_entries'] ) :
		get_template_part( 'template-parts/collection-related' );
	endif;
	?>

	<?php get_template_part( 'template-parts/ad-before-more-from' ); ?>

	<?php
	if ( $bimber_elements['more_from'] ) :
		get_template_part( 'template-parts/collection-more-from' );
	endif;
	?>


	<?php get_template_part( 'template-parts/ad-before-comments' ); ?>

	<?php if ( $bimber_elements['comments'] ) : ?>
		<?php if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif; ?>
	<?php endif; ?>

	<?php get_template_part( 'template-parts/ad-before-dont-miss' ); ?>

	<?php
	if ( $bimber_elements['dont_miss'] ) :
		get_template_part( 'template-parts/collection-dont-miss' );
	endif;
	?>

	<meta itemprop="mainEntityOfPage" content="<?php echo esc_url( get_permalink() ); ?>"/>
	<meta itemprop="dateModified"
	      content="<?php echo esc_attr( get_the_modified_time( 'Y-m-d' ) . 'T' . get_the_modified_time( 'H:i:s' ) ); ?>"/>

	<span itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
		<meta itemprop="name" content="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
		<span itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
			<meta itemprop="url" content="<?php echo esc_url( bimber_get_microdata_organization_logo_url() ); ?>" />
		</span>
	</span>
</article>
