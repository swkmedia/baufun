<?php
/**
 *  The template for displaying featured entries
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_type = bimber_get_theme_option( 'featured_entries', 'type' );

if ( 'none' === $bimber_type ) {
	return;
}

$bimber_time_range = bimber_get_theme_option( 'featured_entries', 'time_range' );

$bimber_title = esc_html__( 'Latest stories', 'bimber' );

switch ( $bimber_type ) {
	case 'most_viewed':
		$bimber_title = esc_html__( 'Most viewed stories', 'bimber' );
		break;

	case 'most_shared':
		$bimber_title = esc_html__( 'Most shared stories', 'bimber' );
		break;
}
?>

<aside class="g1-row g1-row-layout-page g1-featured-row">
	<div class="g1-row-inner">
		<div class="g1-column">

			<h2 class="g1-zeta g1-zeta-2nd g1-featured-title"><?php echo wp_kses_post( $bimber_title ); ?></h2>

			<?php
			// Get built query from cache.
			$bimber_query = get_transient( 'bimber_featured_entries_query' );

			// Build cache if not set.
			if ( false === $bimber_query ) {
				// Common args.
				$bimber_query_args = array(
					'posts_per_page'      => 6,
					'ignore_sticky_posts' => true,
				);

				// Category.
				$bimber_query_args['category_name'] = bimber_get_theme_option( 'featured_entries', 'category' );

				if ( is_array( $bimber_query_args['category_name'] ) ) {
					$bimber_query_args['category_name'] = implode( ',', $bimber_query_args['category_name'] );
				}

				// Tag.
				$bimber_tags = array_filter( bimber_get_theme_option( 'featured_entries', 'tag' ) ); // array_filter removes empty values.

				if ( ! empty( $bimber_tags ) ) {
					$bimber_query_args['tag_slug__in'] = $bimber_tags;
				}

				// Time range.
				$bimber_query_args = bimber_time_range_to_date_query( $bimber_time_range, $bimber_query_args );

				// Type.
				switch ( $bimber_type ) {
					case 'recent':
						$bimber_query_args['orderby'] = 'post_date';
						break;

					case 'most_viewed':
						$bimber_query_args = bimber_get_most_viewed_query_args( $bimber_query_args, 'featured' );
						break;

					case 'most_shared':
						$bimber_query_args = bimber_get_most_shared_query_args( $bimber_query_args );
						break;
				}

				$bimber_query_args = apply_filters( 'bimber_global_featured_entries_query_args', $bimber_query_args );

				$bimber_query = new WP_Query( $bimber_query_args );

				set_transient( 'bimber_featured_entries_query', $bimber_query );
			}

			if ( $bimber_query->have_posts() ) {
				$bimber_settings = apply_filters( 'bimber_global_featured_entry_settings', array(
					'elements' => array(
						'featured_media' => true,
						'avatar'         => false,
						'categories'     => false,
						'title'          => true,
						'summary'        => false,
						'author'         => false,
						'date'           => false,
						'shares'         => false,
						'views'          => false,
						'comments_link'  => false,
					),
				) );

				bimber_set_template_part_data( $bimber_settings );

				?>
				<div class="g1-featured g1-featured-viewport-start">
					<a href="#"
					   class="g1-featured-arrow g1-featured-arrow-prev"><?php esc_html_e( 'Previous', 'bimber' ) ?></a>
					<a href="#"
					   class="g1-featured-arrow g1-featured-arrow-next"><?php esc_html_e( 'Next', 'bimber' ) ?></a>

					<ul class="g1-featured-items">
						<?php while ( $bimber_query->have_posts() ) : $bimber_query->the_post(); ?>

							<li class="g1-featured-item">
								<?php get_template_part( 'template-parts/content-grid-xs', get_post_format() ); ?>
							</li>

						<?php endwhile; ?>
					</ul>

					<div class="g1-featured-fade g1-featured-fade-before"></div>
					<div class="g1-featured-fade g1-featured-fade-after"></div>
				</div>
				<?php

				bimber_reset_template_part_data();
				wp_reset_postdata();
			} else {
				?>
				<div class="g1-featured-no-results">
					<p>
						<?php esc_html_e( 'No featured entries match the criteria.', 'bimber' ); ?><br/>
						<?php esc_html_e( 'For more information please refer to the documentation.', 'bimber' ); ?>
					</p>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<div class="g1-row-background"></div>
</aside>
