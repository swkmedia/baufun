<?php
/**
 * The template part for displaying the featured content.
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_template_data                      = bimber_get_template_part_data();
$bimber_featured_entries                   = $bimber_template_data['featured_entries'];
$bimber_featured_entries['posts_per_page'] = 3;

$bimber_featured_ids = bimber_get_featured_posts_ids( $bimber_featured_entries );

$bimber_query_args = array();

if ( ! empty( $bimber_featured_ids ) ) {
	$bimber_query_args['post__in']            = $bimber_featured_ids;
	$bimber_query_args['orderby']             = 'post__in';
	$bimber_query_args['ignore_sticky_posts'] = true;
}

$bimber_title = wp_kses_post( __( 'Latest <span>stories</span>', 'bimber' ) );

switch ( $bimber_featured_entries['type'] ) {
	case 'most_shared':
		$bimber_title = wp_kses_post( __( 'Most <span>shared</span>', 'bimber' ) );
		break;

	case 'most_viewed':
		$bimber_title = wp_kses_post( __( 'Most <span>viewed</span>', 'bimber' ) );
		break;
}

$bimber_query = new WP_Query( $bimber_query_args );
?>

<?php if ( $bimber_query->have_posts() ) {
	$bimber_index = 0;

	bimber_set_template_part_data( $bimber_featured_entries );

	?>

	<section class="g1-row g1-row-layout-page archive-featured archive-featured-row">
		<div class="g1-row-inner">
			<div class="g1-column">

				<h2 class="g1-delta g1-delta-2nd archive-featured-title"><?php echo wp_kses_post( $bimber_title ); ?></h2>
				<div class="g1-mosaic">
					<?php while ( $bimber_query->have_posts() ) : $bimber_query->the_post();
						$bimber_index ++; ?>

						<div class="g1-mosaic-item-<?php echo absint( $bimber_index ); ?>">
							<?php
							if ( 1 === $bimber_index ) {
								get_template_part( 'template-parts/content-tile-xl', get_post_format() );
							} else {
								get_template_part( 'template-parts/content-tile-standard', get_post_format() );
							}
							?>
						</div>

					<?php endwhile; ?>
				</div>
			</div>
		</div>
		<div class="g1-row-background"></div>
	</section>

	<?php

	bimber_reset_template_part_data();
	wp_reset_postdata();
}
