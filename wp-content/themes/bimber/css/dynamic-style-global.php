<?php
/**
 * Global styles
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

$bimber_filter_hex = array( 'options' => array( 'regexp' => '/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/' ) );

$bimber_page_layout = bimber_get_theme_option( 'global', 'layout' );

$bimber_page_width = absint( bimber_get_theme_option( 'global', 'width' ) );
$bimber_page_width = $bimber_page_width > 1210 ? $bimber_page_width : 1210;

$bimber_body_background          = array();
$bimber_body_background['color'] = new Bimber_Color( bimber_get_theme_option( 'global', 'background_color' ) );

$bimber_cs_1_background = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_background_color' ) );
$bimber_cs_1_background_variations = bimber_get_color_variations( $bimber_cs_1_background );
$bimber_cs_1_background_5          = new Bimber_Color( $bimber_cs_1_background_variations['tone_5_90_hex'] );
$bimber_cs_1_background_10         = new Bimber_Color( $bimber_cs_1_background_variations['tone_20_20_hex'] );

$bimber_cs_1_text1                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text1' ) );
$bimber_cs_1_text2                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text2' ) );
$bimber_cs_1_text3                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text3' ) );
$bimber_cs_1_accent1                = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_accent1' ) );
$bimber_cs_2_background             = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_2_background_color' ) );
$bimber_cs_2_text1                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_2_text1' ) );
?>
body.g1-layout-boxed {
background-color: #<?php echo filter_var( $bimber_body_background['color']->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.g1-layout-boxed .g1-row-layout-page {
max-width: <?php echo intval( $bimber_page_width ); ?>px;
}

/* Global Color Scheme */
.g1-row-layout-page > .g1-row-background,
.g1-sharebar > .g1-row-background,
.g1-content > .g1-background,
.g1-current-background {
background-color: #<?php echo filter_var( $bimber_cs_1_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

input,
select,
textarea {
border-color: #<?php echo filter_var( $bimber_cs_1_background_10->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}


h1,
h2,
h3,
h4,
h5,
h6,
.g1-mega,
.g1-alpha,
.g1-beta,
.g1-gamma,
.g1-delta,
.g1-epsilon,
.g1-zeta,
blockquote,
.g1-link,
.g1-quote-author-name,
.g1-links a,
.entry-share-item,
.entry-print,
.g1-nav-single-prev > a > strong,
.g1-nav-single-next > a > strong,
.widget_recent_entries a,
.widget_archive a,
.widget_categories a,
.widget_meta a,
.widget_pages a,
.widget_recent_comments a,
.widget_nav_menu .menu a {
color: #<?php echo filter_var( $bimber_cs_1_text1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

body {
color: #<?php echo filter_var( $bimber_cs_1_text2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.entry-meta {
color: #<?php echo filter_var( $bimber_cs_1_text3->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.entry-meta a {
color: #<?php echo filter_var( $bimber_cs_1_text1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}


a,
.entry-title > a:hover,
.entry-meta a:hover,
.menu-item > a:hover,
.current-menu-item > a,
.mtm-drop-expanded > a,
.g1-nav-single-prev > a:hover > strong,
.g1-nav-single-prev > a:hover > span,
.g1-nav-single-next > a:hover > strong,
.g1-nav-single-next > a:hover > span,
.g1-pagination-item-next a.next:after,
.g1-pagination-item-prev a.prev:before,
.mashsb-main .mashsb-count {
color: #<?php echo filter_var( $bimber_cs_1_accent1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

input[type="submit"],
input[type="reset"],
input[type="button"],
button,
.g1-button-solid,
.g1-button-solid:hover,
.entry-categories .entry-category:hover,
.entry-counter,
.author-link,
.author-info .author-link,
.g1-box-icon {
border-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #<?php echo filter_var( $bimber_cs_2_text1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}



.g1-button-simple,
input.g1-button-simple,
button.g1-button-simple {
	border-color: currentColor;
	background-color: transparent;
	color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}



.g1-drop-toggle-arrow {
color: #<?php echo filter_var( $bimber_cs_1_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}



